﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mvc.Controllers
{
    public class HelloController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Welcome(string name = "Anon", int numtimes = 1)
        {
            ViewData["Message"] = "Hello, " + name;
            ViewData["numtimes"] = numtimes;
            return View();
        }
    }
}
