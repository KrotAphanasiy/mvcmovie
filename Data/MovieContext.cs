﻿using Microsoft.EntityFrameworkCore;
using mvc.Models;

namespace mvc.Data
{
    public class MovieContext : DbContext
    {
        public MovieContext(DbContextOptions<MovieContext> options):
            base(options)
        {

        }
        
        public DbSet<Movie> Movie { get; set; }

    }
}
